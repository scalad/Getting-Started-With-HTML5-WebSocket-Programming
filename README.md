<h1 align="center">Getting-Started-With-HTML5-WebSocket-Programming</h1>
</br>
<h2 align="center">
开发并部署你的第一个安全的、可扩展的实时Web应用程序
	<h3 align="center">
		作者@Vangos Pterneas<br/> 
		版权©2013 Pack出版
	</h3>
</h2>

### 目录

#### 第一章 与WebSocket握手

* WebSocket出现前所使用的技术
	* 轮询
	* 长轮询
	* 流
	* AJAX与回调
* HTML5的到来
* WebSocket 协议
* 支持移动端？
* 未来的幻想
* 我们将要做什么？
* 总结

<h4>第二章 WebSocket 接口</h4>

* HTML5基础
	* 标记
	* 风格
	* 逻辑
* 聊天应用
* API预览
	* 浏览器支持
	* WebSocket对象
	* 事件
		* onopen
		* onmessage
		* onclose
		* onerror
	* 动作
		* send()
		* close()
	* 属性
	* 完整的例子
		* index.html
		* chat.js
	* 服务器
* 总结 

<h4>第三章</h4>

* 为什么需要WebSocket服务器
* 创建服务器
	* 选择适合你的技术
		* C/C++
		* Java
		* .NET
	 	* PHP
		* Python
		* Ruby
		* JavaScript
	* 配置开发环境
* 连接到Web服务器
	* 创建WebSocket服务器实例
	* Open
	* Close
	* Message
	* Send
* 其他方法
* 完整代码
* 总结

<h4>第四章 数据传输-发送、接收以及解码</h4>

* WebSockets可以传输哪种类型的数据
	* String
		* JSON
		* XML
	* ArrayBuffer
		* 视频流
* 汇总
	* 使用JSON发送消息
	* 向服务器发送图片
* 总结

<h4>第五章 安全</h4>

* WebSocket请求头
* 常见的攻击
	* 拒绝式服务器攻击
	* 中间人攻击
	* XSS攻击
* WebSocket本地防御机制
	* SSH/TLS
	* 客户端-服务器伪装
* 安全工具箱
	* Fidder
	* Wireshark
	* 浏览器开发工具
	* ZAP
* 总结

<h4>第六章 错误处理和回调机制</h4>

* 错误处理
	* 检查网络是否可用
* 回调机制
	* JavaScript polyfills
		* 流行的polyfills
	* 浏览器插件
* 总结

<h4>第七章 走向移动端(同样指平板)</h4>

* 为什么谈手机问题
	* 本地移动app与移动网络对比
	* 前提
	* 安装SDK
	* 在移动端浏览器测试代码
* 本地
	* 创建工程
	* 创建WebSocket iPhone app
* 关于在iPad的应用？
* 总结

<h4>附录</h4>

* 资源
	* 在线资源
	* 文章
* 源代码
* 系统需求
* 保持联系
* 索引

<h2 align="center">第一章 与WebSocket握手</h2>

在真实的生活中，握手通常是指两个人的相互握住并做一个简短的上下晃动的动作。如果你曾经有通过这种方式来来感谢某个人，那么你已经明白了HTML5 WebSocket协议的基本概念。

WebSocket定义了一种在web服务器和客户端持久性全双工的通讯方式，这意味着服务器和客户端可以在同一时刻进行数据的交换。WebSocket真正的引入了并发性，并且为了能获得高性能的服务对它做了优化，这才让我们看见了更加具有响应式和更加丰富的web应用。

下面的图显示了一个服务器与多个客户进行握手交互：

![](https://github.com/scalad/Getting-Started-With-HTML5-WebSocket-Programming/blob/master/image/chapter1.png)

正式的说一下，WebSocket协议已经被Internet Engineering Task Force(IETF)作为一种标准化并且对于web浏览器的WebSocket API目前也正在逐步被Word Wide Web Consortium(W3C)标准化，没错，它正在进行中(目前已经成为标准了)。当前规范已经发表为“建议标准”了，因此你不必担心未来会发生重大的变化。

<h4>WebSocket出现前所使用的技术</h4>

在讨论WebSocket之前，让我们看一看有哪些技术被用于服务器和客户端之间通信。

<h4>轮询</h4>
Web工程师最初在处理这个问题时是使用了一种称为轮询的技术。轮询是一个同步方法(意味着没有并发)周期性的执行请求，即便没有数据在传输。客户端在指定的时间间隔内不断的创建请求，每一次，服务器响应给客户端一些可用的数据或者是警告信息。因此，轮询是“一直在工作”，在多数情况下这个方法是多余的并且它在现代的Web应用程序中是非常消耗资源的。

<h4>长轮询</h4>
长轮询和轮询是一个相似的技术，但正如它名字所表明的那样子，客户端打开一个连接然后服务器一直保持这个连接存活直到客户端获取到数据或者是该连接超时。之后客户端可以重新执行一次上述的请求，依次重复。长轮询是在轮询性能上的改善，但是不断的请求也减缓了整个过程。

<h4>流技术</h4>
流看起来是实时数据传输最好的选择，当使用流技术的时候，客户端执行一个请求然后服务器将这个链接无限期的保持打开状态，当准备好之后就可以获取数据了。虽然这是个巨大的改进，但是流技术依然包含了HTTP的请求头，它会使请求文件增大并引起一些不必要的延迟。

<h4>回调与AJAX</h4>
现在的web已经建立在HTTP的请求-响应模式下，HTTP是一种无状态的协议，这意味着服务器和客户端两部分的通信是独立请求和响应的。用通俗的话来讲，客户端向服务器获取一些信息，服务器通常以HTML的文档方式响应给客户端，然后客户端的页面刷新(这实际上称为回调)。在它们两个之间什么也没发生，直到一个新的请求发生(例如点击一个按钮或者是选择一个下拉按钮)。但是界面刷新时的闪烁是非常恼人的(专业术语来讲叫做“用户体验”)。

直到2005年由回调所产生的闪烁才因为Asynchronous JavaScript and XML(AJAX)所解决。AJAX是基于JavaScript的XmlHttpRequest对象以及允许异步执行JavaScript代码而不影响用户界面。AJAX只发送和接收部分的网页而不需要加载整个网页。

想象一下你如果你想使用FaceBook在你的时间轴上发表评论，你更新了界面上一些文本字段的状态，回车…好吧!你的评论将自动的发布更新而不需要页面的加载。除非FaceBook使用AJAX，不然浏览器为了显示你新的状态将需要刷新整个页面。

AJAX总是伴随着非常流行的像jQuery的JavaScript类库，它在最终的用户体验上有着很大的改进，并且它被广泛的认为它是每个网站都必备的属性。正是由于AJAX出现之后JavaScript才被大多数人所认可。

但这些还不够。长轮询是一项非常有用的技术，它使你的浏览器看起来像是维持了一个持久性的链接，而事实上是浏览器进行连续不断的请求！这是非常消耗资源的，特别对于速度和数据的大小非常重要的移动设备上。

上面所提到的所有方法都提供了实时双向通讯，但是跟WebSocket对比它们任然有三个很明显的缺陷：

* 它们发送完整的HTTP请求头，使得总文件的大小更大
* 它们的通讯是半双工通讯，意味着双方(客户端/服务器)必须等待另一方完成
* Web服务器消耗更多的资源

回调的方式很像一部对讲机，你需要等待另一方完成通话(半双工方式)，而WebSocket可以同时说话(全双工方式)!

网络最初是为显示文本文档而构建的，但请想想今天它是如何使用的。我们可以发送不同于文本的数据，例如播放媒体内容、添加本地功能以及完成复杂的任务等等。AJAX和浏览器插件例如Flash是非常实用的，但是我们应该更多的考虑下当今的需求，如今我们使用网络的方式是需要一个整体的新的应用开发框架。

<h4>HTML5的到来</h4>

HTML5为前面讨论的话题引入了重要的解决方案，现在它已经成为一项丰富而又合理的技术，如果你已经对HTML5相当熟悉，那么请随意跳过本节继续阅读。

HTML5是一个用于开发和设计Web应用程序的健壮的框架。

HTML5不仅仅不是一种新的标记语言或者是一些新的格式选择器，它也不是一门新的语言。HTML5代表了编程语言、工具和技术的集合，它们每一个都是HTML5中一个相互分离的角色，但是这些又完成某个特定的任务，即为任何类型的设备构建丰富的网络应用程序。

HTML5最主要的支柱包括了Markup，CSS3,和JavaScript APIs等等，下图显示了HTML5的组件：

![](https://github.com/scalad/Getting-Started-With-HTML5-WebSocket-Programming/blob/master/image/chapter1-2.png)

下面是HTML5家族中的主要成员。这本书并没有涵盖所有HTML5集合，所以我建议你参考html5rocks.com并实践一下里面写的小例子。

#
	Markup 			Structural elements
					Form elements
					Attributes
	Graphics		Style sheets
					Canvas
					SVG
					WebGL
	Multimedia 		Audio
					Video	
	Storage			Cache
					Local storage
					Web SQL
	Connectivity	WebMessaging
					WebSocket
					WebWorkers
	Location    	Geolocation
#

虽然存储和连接应该是最热门的话题，但是如果你是一个有经验的web开发者你比不担心。通过本书，我们会解释怎样完成常见的任务并且我们会一步步创建一些例子，你可以下载并运行测试。此外，通过HTML5 API管理WebSocket是非常容易的，所以请深吸一口气，不要担心潜水。

<h4>WebSocket 协议</h4>

WebSocket重头开始定义了全双工通讯协议，实际上，WebSokets与WebWorkers一起为浏览器丰富的桌面付出了巨大的努力。

<h4>URL</h4>

HTTP协议需要它自己的模式(HTTP和HTTPS)，所以WebSocket协议也一样。下面是典型的WebSocket URL的例子：

	ws://example.com:8000/chat.php

首先需要注意的是ws前缀，这是很正常的，因为我们需要一个新的UTL模式的新的协议，wss也是支持的，是WebSocket相当于HTTPS的安全连接(SSL),网址的奇遇部分类似于旧的HTTP URL，如下图所示。

下图显示了令牌中的URL：

![](https://github.com/scalad/Getting-Started-With-HTML5-WebSocket-Programming/blob/master/image/chapter1-3.png)

<h4>支持的浏览器</h4>

目前，WebSocket协议的最新规范是RFC 6455，期待未来的最后一个版本现代的每个浏览器都可以支持它，更具体的说，以下的浏览器支持RFC 6455：

* Internet Explorer 10+
* Mozilla Firefox 11+
* Google Chrome 16+
* Safari 6+
* Opera 12+

值得一提的是，Safari（iOS），Firefox（Android），Chrom（Android，iOS）和Opera Mobile的移动版本都支持WebSockets，它们将WebSocket的功能带入了智能手机和平板电脑。

可是，等等，全世界任然有很多人在使用旧版本的浏览器该怎么办呢？好的，你也没必要担心，因为在这本书里面，我们将看看可以使用哪些后备的技术来使我们的网站可以支持最大的用户访问。

<h4>谁在使用WebSockets</h4>

虽然WebSocket是一项赞新的技术，有相当多的有前景的公司利用该技术的各种功能给用户带来了更加丰富的体验。最著名的例子是Kaazing公司(http://demo.kaazing.com/livefeed),这家创业公司在实时通讯的平台上投资了超过1700万美元。

其他公司如下：

	名字   	        网站								描述
	Gamooga			http://www.gamooga.com/  		为apps和游戏系统后台实时服务 
	GitLive    	 	http://gitlive.com/          	项目在GitHub上的通知服务
	Superfeedr    	http://superfeedr.com       	实时数据推送
	Pusher       	http://pusher.com           	为web和app提供可扩展的实时API
	Smarkets      	https://smarkets.com/        	实时投注系统
	IRC Cloud     	https://www.irccloud.com/   	实时聊天

下面是两个包含了大量的WebSocket的演示用例地址：
	
	http://www.websocket.org/demos.html
	http://www.html5rocks.com/en/features/connectivity

<h4>支持移动端</h4>

正如WebSocket名字所表示的一样，WebSocket是和Web相关的。但是我们知道，网络不仅仅是和浏览器相关的一些技术，相反的，它是包括了台式电脑、智能手机和平板电脑等大量设备的广泛通信平台。

明显的，任何基于HTML5的app都可以运行在任何支持HTML5运行的web手机浏览器上。假若你想使用WebSocket来增强本地移动app的功能，那么WebSocket是否支持现代主流的一栋操作系统呢？简单的回答是：可以。目前，目前，移动行业（苹果，谷歌，微软）所有主要的厂商提供WebSocket API，让你可以在自己的本机应用程序中使用。 并且iOS，Android和Windows智能手机以及平板电脑以类似于HTML5的方式集成WebSockets。

<h4>未来的幻想</h4>

新的神经科学研究证实了关于握手的力量的古老谚语：(http://www.sciencedaily.com/releases/2012/10/121019141300.htm) 人们在握手问候的时候确实给彼此形成更好的印象。正如人们可以通过握手可以有更好的效果。我们通过调查用户体验作为性能（让用户少等待）和简单性（开发人员快速开发）为指标知道，WebSocket同样也带给用户更好的用户体验。

所以，这取决于你：你想构建更现代化、真正的实时web应用程序？您是否希望为用户提供最大的体验？您是否想为您现有的网络应用程序提供卓越的性能提升？如果这些问题的答案都是肯定的，那么是时候认识到WebSocket API已经足够成熟，足以在这里提供它的作用。

<h4>我们将要做什么？</h4>

通过这本书，我们将实现一个真实的实时通信的项目：一个简单的、多用户的、基于WebSocket的聊天应用系统。在线聊天是所有现代社交网络中非常常见的功能。我们将一步步学习如何配置Web服务器，实现HTML5客户端以及在它们之间传输消息.

除了纯文本消息，我们将看到WebSockets如何处理各种类型的数据，如二进制文件，图像和视频， 是的，我们也将演示实时媒体流！

此外，我们也将提高我们的应用程序的安全性，检查一些已知的安全风险，并找出如何避免常见的陷阱。进一步，我们将针对那些不支持（或不想）的浏览器的可怜的家伙采取一些后备技术来支持WebSocket。

最后,但这并非不重要的，我们会了解移动端。 你可以使用桌面浏览器，手机或平板电脑进行聊天。如果你可以在多个目标上使用相同的技术和原则，那不是很好吗？ 好吧，通过阅读这本书，你会发现如何轻松地将您的网络应用程序转换为本机移动和平板电脑应用程序。

<h4>总结</h4>

在第一章我们介绍了WebSocket协议，提到了现有的实时通信技术并确定了使用WebSocket实现的具体需求，此外，我们确定了它与HTML5的关系，并说明了用户可以从该技术获取到什么，现在是时候更加详细的介绍WebSocke客户端的API了。

<h2 align="center">第二章 WebSocket API</h2>

如果你熟悉HTML和JavaScript，那么你已经知道足够的东西，现在可以开始开发HTML5 WebSockets了。WebSocket通信和数据传输是双向的，因此我们需要建立两个部分：一个服务器和一个客户端。本章重点介绍HTML web客户端并介绍WebSocket客户端的API。

<h4>HTML5基础</h4>

任何HTML5 Web客户端是结构，样式和编程逻辑的组合,正如我们已经提到的，HTML5框架在每次使用的时候提供了一组离散的技术。虽然我们假设你已经很熟悉这些概念，但还是让我们快速的看看他们。

<h4>标记</h4>

标记语言定义了你的ewb应用程序的结构，它是一组XML标记文档，可以让你指定他们在HTML文档中的层次结构。新的HTML热门标签包括header、article、footer、aside和nav等标签，这些标签都有特定的含义，有助于区分Web文档中的不同部分。

下面是一个简单的HTML5标记代码示例，它是我们的聊天应用程序生成基本元素：一个文本框、两个按钮、一个标签，文本框用来输入消息，第一个按钮用来发送消息，第二个按钮用来终止聊天，标签用来显示服务器发送的交互信息：

```HTML
<!DOCTYPE html>
<head>
   <title>HTML5 WebSockets</title>
The WebSocket API
</head>
<body>
   <h1> HTML5 WebSocket chat. </h1>
   <input type="text" id="text-view" />
   <input type="button" id="send-button" value="Send!" />
   <input type="button" id="stop-button" value="Stop" />
   <br/>
   <label id="status-label">Status</label>
</body>
```

上面代码的第一行（DOCTYPE）表示我们使用的是最新版本的HTML，即HTML5。
有关HTML5标记语言的更多信息，请参考：http://html5doctor.com/, 这里有一个完整的HTML5所支持的标记文档：http://html5doctor.com/element-index.。

<h4>样式</h4>

为了显示颜色、背景、字体、对齐方式等等，你需要熟悉样式表(CSS)。CSS相当好理解，比如，如果你想改变标题的风格(例如颜色、对齐方式和字体)，你会编写类似下面的代码：

```css
	h1 {
	 color: blue;
	 text-align: center;
	 font-family: "Helvetica Neue", Arial, Sans-Serif;
	 font-size: 1em;
	}
```

http://www.css3.info/ 是个很好的CSS3资源，你可以进一步阅读。

<h4>逻辑</h4>

标记语言定义了结构，CSS规则应用了样式，那什么是事件处理和用户操作？好吧，那么JavaScript来了！JavaScript是一种脚本编程语言，它允许你根据操作控制的行为来更改Web应用程序。使用JavaScript，你可以处理按钮点击、页面加载、应用额外的风格、添加特殊的效果甚至可以从web服务上获取数据。使用JavaScript，您可以创建对象，并为其分配属性和方法，可以在发生事件时提升和捕获事件。

以下是一段简单的JavaScript例子：

```javascript
	var buttonSend = document.getElementById("send-button");
	buttonSend.onclick = function() {
	 	console.log("Button clicked!");
	}
```

第一行在文档树中进行查找，找到名字为`action-button`的元素并存储在名字为`buttonSend`的对象中，然后，将一个函数分配给按钮的onclick事件，每次点击按钮的时候这个函数都会被执行。

新的HTML5功能主要基于JavaScript，因此在开发任何Web应用程序之前，必须掌握JavaScript的基本知识。 最重要的是，WebSocket API也是纯JavaScript！

<h4>聊天应用程序</h4>

最流行的全双工通信应用是聊天应用程序，我们将在这里开发一个简单的聊天应用程序。首先要做的事情是配置一下客户端，它有三个基本的文件组成：

* HTML(.html)文件包含了结构化标记语言的web页面
* CSS(.css)文件包含了所有的样式信息
* JavaScript(.js)包含了应用程序逻辑的JavaScript文件

目前，这些已经包含了你创建一个全功能的HTML聊天客户端的所有文件，而不需要安装浏览器插件或者其他的外部库。

<h4>API预览</h4>

API代表应用程序接口(Application Programming Interface),它是一组对象、方法以及规则的集合，让你与底层的功能进行交互。思考一下WebSocket的协议，它的API包含了WebSocket主对象、事件、方法和属性。

将且将这些特性称为操作，WebSocket API允许你连接到本地或远程服务器，监听消息，发送数据和关闭连接。下面是WebSocket API典型的用法：

* 检查浏览器是否支持WebSocket->
* 创建一个WebSocket实例
* 连接到WebSocket服务器
* 注册WebSocket事件
* 根据用户的动作执行正确的数据传输
* 关闭连接

<h4>浏览器支持</h4>

WebSocket协议是HTML5的新特性，所以并不是所有的浏览器都支持。如果你曾经试图在浏览器上运行特定的WebSocket代码，它是什么也不会发生的，也就是不支持的。想想你的用户：他们是不会想在没有反应的网上浏览的，此外，你也不想错误任何潜在的用户。

因此，您应该在运行任何WebSocket代码之前检查浏览器兼容性，如果浏览器无法运行WebSocket代码，你应该提供错误的消息或者是一个例如AJAX或基于Flash函数的回调函数。在第六章会有更多关于回调函数和错误处理的信息，我也喜欢提供消息悄悄告诉我的用户更新他们的浏览器。

JavaScript提供了一个简单的方法检测浏览器是否支持特定的WebSocket代码：

```javascript
if (window.WebSocket) {
 console.log("WebSockets supported.");
 	// Continue with the rest of the WebSockets-specific functionality…
}else {
 	console.log("WebSockets not supported.");
 	alert("Consider updating your browser for a richer experience.");
}

```

`window.WebSocket`表达式指出了WebSocket协议是否在浏览器中实现，下面的表达也是等效的：

	Window.websocket
	“WebSocket” in window
	Window[“webSocket”]

它们每个验证都是相同的，你也可以使用浏览器自带的开发者工具进行支持的功能的检查。

如果想要看哪些浏览器支持WebSocket协议，这里有最新资源[http://caniuse.com/#feat=websockets](http://caniuse.com/#feat=websockets)

在写这本书的时候，WebSocket已经完全支持Internet Explorer 10+,Firefox 20+、Chrome 26+、Safari6+、 Opera 12.1+、 Safari for iOS 6+、以及 Blackberry。

#### WebSocket对象

现在是时候做一个初始化连接到服务器了。而我们所需要的只要创建一个提供了连接到远程或者是本地服务器的URLd的WebSocket的JavaScript对象：

```javascript
	Var socket = new WebSocket(“ws://echi.websocket.org”)
```

当初始化了WebSocket对象，它会马上在特定的服务器上打开一个连接。在第三章的服务器配置上会详细的介绍我们怎样开发服务器端的程序。现在，我们有必要记住这个有效的WebSocket URL地址。

Ws://echo.websocket.org这个URL例子是一个公共开放的地址，我们可以用来进行测试和实验。Websocket.org的服务器是一直运行着的，当它接收到消息后会把这个消息再返回给客户端！我们需要做的只是确保我们的客户端正常运行。

#### 事件

在创建了WebSocket对象之后，我们需要处理它所开放的事件。在WebSocket的API中有四个主要的事件：Open，Message，Close和Error。你可以分别通过实现onopen,onmessage,onclose和onerror函数来处理它们，或者使用addEventListener方法。这两种方法都可以实现我们需要做的事情，但是第一种方法看起来更加清晰。

注意，很明显的，我们提供给事件的方法并不会周期性的执行，它们只会在特的事件发生的时候异步执行。因此，让我们进一步的探究它们。

#### Onopen

Onopen事件在链接成功建立后立即被触发，这意味着是在客户端和服务器之间的初始握手导致成功后的第一次调用，并且应用程序现在可以发送数据了。

```javascript
	socket.onopen = function(event) {
		console.log("Connection established.");
		// Initialize any resources here and display some user-friendly messages.
	 	var label = document.getElementById("status-label");
	 	label.innerHTML = "Connection established!";
	}
```

在用户等待连接打开时向用户提供适当的反馈是一个很好的做法，WebSocket虽然绝对快，但是因特网的连接可能很慢！

<h4>onmessage</h4>

onmessage事件相对于服务器来说是客户端的耳朵，每当服务器发送一些数据，onmessage事件会被触发，数据可能包含一些文本，图像或者二进制数据，这取决于你如何解释和可视化这些数据。

```javascript
	Socket.onmessage = function(event) {
		Console.log(“Data received!”);
	}
```

检查数据的类型是相当容易的，下面是我们如何显示响应得到字符串：

```javascript
	Socket.onmessage = function(event) {
	if (typeof event.data === "string") {
	 // If the server has sent text data, then display it.
	 var label = document.getElementById("status-label");
	 label.innerHTML = event.data;
	 }
	}
```

在第四章的数据传输-发送，接收和解码我们将会学习更多支持类型的数据。

<h4>onclose</h4>

onclose事件标志着会话的结束。每当此事件发生时，在服务器和客户端双方都不能够再发送消息，除非再次打开链接。连接可能由于多种原因而终止,它可以被服务器关闭，它可能被客户端使用close（）方法关闭，或者因为TCP错误而关闭。

您可以通过检查事code、reason和wasClean参数来轻松检测连接关闭的原因。code参数为您提供一个唯一的数字，表示中断的起因，reason参数提供了对中断的描述的格式化字符串，最后，wasClean参数表明这个连接关闭是由于服务器的原因还是不可见的网络原因所引起的，以下代码段说明了这些参数的正确使用方式：

```javascript
socket.onclose = function(event) {
	 console.log("Connection closed.");
	 var code = event.code;
	 var reason = event.reason;
	 var wasClean = event.wasClean;
	 var label = document.getElementById("status-label");
	 if (wasClean) {
	 label.innerHTML = "Connection closed normally.";
	 } else {
	 label.innerHTML = "Connection closed with message " + reason +
	 "(Code: " + code + ")";
 }
```

您可以在本书的附录中找到代码值的详细列表。

#### onerror

onerror事件在发生一些错误时触发(意想不到的行为或者失败)，注意，onerror事件总是意味着一个连接的终止，这是一个关闭事件。

```javascript
socket.onclose = function(event) {
 		console.log("Error occurred.");
 // Inform the user about the error.
 var label = document.getElementById("status-label");
 label.innerHTML = "Error: " + event;
}
```

#### 动作

事件在发生某些事情的时候被触发。当我们想要某些事情发生时我们需要显示的触发一些动作(或者调用方法)！WebSocket写提供了两个主要的方法：send()和close()。

#### send()

当连接是打开时，你可以和服务器相互发送消息，send()方法允许你向服务器发送各种类型的数据。这里是我们如何发送聊天消息（实际上，是HTML中输入框的内容）给聊天室中的每个人：

```javascript
 // Find the text view and the button.
 var textView = document.getElementById("text-view");
 var buttonSend = document.getElementById("send-button");
 // Handle the button click event.
 buttonSend.onclick = function() {
 // Send the data!!!
 socket.send(textView.value);
```

它是如此的简单！

但是等等…上面的代码并不是100%正确。请记住只有脸连接打开的情况下你才可以发送消息，这意味着我们需要在onopen事件处理程序中放置send()方法，或者检查readyState属性，这个属性返回WebSocket的连接状态。所以，应该相应的修改前面的代码片段：

```javascript
button.onclick = function() {
		// Send the data if the connection is open.
		if (socket.readyState === WebSocket.OPEN) {
			socket.send(textView.value);
 		}
}
```

在发送所需的数据后，你可能需要等待服务器的交互或关闭连接，在我们的演示示例中，我们保持连接打开，除非点击停止按钮来关闭连接。

#### close()

close()方法代表了握手的结束，它关闭了和服务器端的连接，除非再次打开连接，不然无法再次传送数据。

与之前的例子很相似，当用户点击了第二个按钮时我们可以调用close()方法：

```javascript
var textView = document.getElementById("text-view");
var buttonStop = document.getElementById("stop-button");
buttonStop.onclick = function() {
 	// Close the connection, if open.
 if (socket.readyState === WebSocket.OPEN) {
 	socket.close();
 }
}
```

我们也可以通过我们上面提到的code和reason参数来关闭：

	Socket.close(1000, “Deliberate disconnection”);

#### 属性
WebSocket对象暴露了一些属性值来让我们了解它具体的特征，我们已经见到了它的readyState属性，下面是WebSocket其余的属性：

| 属性           | 描述                                         |
| ------------- |:--------------------------------------------:|
| url           | 返回WebSocket的URL                            |
| protocol      | 连接状态的报告并且可以携带的值                   |
|               | WebSocket.OPEN                               |
|               | WebSocket.CLOSE                              |
|               | WebSocket.CONNECTION                         |
|               | WebSocket.CLOSING                            |
| buffereAmount | 返回send()方法被调用时数据bytes数量             |
| binaryType    |返回onmessage()触发时接收到的格式化的二进制数据    |

#### 完整的例子

下面是我们使用的完整的HTML和JavaScript代码，我们省略了样式表文件以确保是要功能的简单。但是，[http://pterneas.com/books/websockets/](http://pterneas.com/books/websockets/) 你可以在这里下载源代码。

#### index.html
```html
	<!DOCTYPE html>
<html>
<head>
 <title>HTML5 WebSockets</title>
 <link rel="stylesheet" href="style.css" />
 <script src="chat.js"></script>
</head>
<body>
 <h1> HTML5 WebSocket chat. </h1>
 <input type="text" id="text-view" />
 <input type="button" id="send-button" value="Send!" />
 <input type="button" id="stop-button" value="Stop" />
 </br>
 <label id="status-label">Status</label>
</body>
</html>
```

#### chat.js
```javascript
window.onload = function() {
var textView = document.getElementById("text-view");
 	var buttonSend = document.getElementById("send-button");
	var buttonStop = document.getElementById("stop-button");
	var label = document.getElementById("status-label");
 	var socket = new WebSocket("ws://echo.websocket.org");
 	socket.onopen = function(event) {
 		label.innerHTML = "Connection open";
 	}
socket.onmessage = function(event) {
 		if (typeof event.data === "string") {
 			label.innerHTML = label.innerHTML + "<br />" + event.data;
		}
 	 }
 socket.onclose = function(event) {
 var code = event.code;
 var reason = event.reason;
 var wasClean = event.wasClean;
 if (wasClean) {
	  		 label.innerHTML = "Connection closed normally.";
 		 } else {
			 label.innerHTML = "Connection closed with message: " +
 	reason + " (Code: " + code + ")";
 }
 }
 socket.onerror = function(event) {
	 label.innerHTML = "Error: " + event;
 }
buttonSend.onclick = function() {
		if (socket.readyState == WebSocket.OPEN) {
			socket.send(textView.value);
		} 
}
buttonStop.onclick = function() {
 if (socket.readyState == WebSocket.OPEN) {
	 socket.close();
 }
 	}
}
```

#### 服务器
你可能注意到我们使用echo.websocket.org作为这个例子的服务器，这个公共的服务器只是简单的返回你发送的数据。在下一章，我们将会构建我们自己的WebSocket服务器并开发真正的聊天app应用。

#### 总结
在这一章，我们构建了我们第一个客户端的WebSocket应用！我们介绍了WebSocket对象以及解释了它的各种方法、事件以及属性。我们还用几行HTML和JavaScript代码开发了一个基本的聊天客户端，正如你在当这个例子中注意到的，它只有一个虚拟服务器回显消息，请继续阅读，你将了解到我们如何配置自己的WebSocket服务器。

## 第三章 服务器配置
WebSocket代表了双向的全双工的通信协议，正因如此，对于这种类型的会话是我们需要两个部分。在前面的一章中，我们实现了WebSocket的客户端应用，现在是时候建立另一端的管道了，它就是WebSocket服务器。

#### 为什么需要WebSocket服务器
我们假设你已经对服务器有了一定的了解，服务器只是具有特定硬件和软件需求的远程计算机，以实现高可用性和长运行时间，增强安全性以及多个对并发连接的管理。

WebSocket服务器只是一个能够处理WebSocket事件和动作的简单程序，它通常暴露类似的方法给WebSocket客户端API并且大多数编程语言提供了实现。下图说明了WebSocket服务器和WebSocket客户端之间的通信过程，强调了触发的事件和操作。下面的图显示了WebSocket服务器和客户端之间事件的触发：

![](https://github.com/scalad/Getting-Started-With-HTML5-WebSocket-Programming/blob/master/image/chapter3-1.png)

这里不要感到困惑——Web服务器可以运行在Apache或IIS上面或者它也可以是在一个与本身完全不同的应用程序。

考虑到硬件条件，你可以使用超级计算机或者是你的电脑作为服务器，但这要考虑到你的项目的需求和预算。

#### 建立服务器
虽然建立服务器需要相关的知识，但是从头开始搭建一个WebSocket的服务器并不困难，但这远离了本书的目的。因此，我们将使用当前已经存在的实现了WebSocket服务器，由于在社区上有大量的开发人员，我们可以轻松地选择我们首选的编程语言或基于WebSocket框架的服务器，甚至大多数实现的WebSocket接口的服务器都是开源的，因此你如果需要的话甚至可以根据你的需要进行修改。

#### 选择适合你的技术
我们听说过一些流行实现了WebSocket的服务器，但在你选择之前这里有几个问题：

* 你最熟悉哪种技术？
* 你的工程有没有特殊的需求？
* 你是否已经有了一个增强的解决方案？
* 服务器是否有活动的社区支持？

现在让我们来看看最广泛使用的编程语言以及最受欢迎的WebSocket服务器类库

#### C/C++

	Tufao						https://github.com/vinipsmaker/tufao
	Wslay 					 	http://wslay.sourceforge.net/
	Libwebsockets 				http://libwebsockets.org/trac
	Mongoose 				 	https://code.google.com/p/mongoose/

#### Java

	Apache Tomcat 				http://tomcat.apache.org/
	JBoss 					 	http://www.jboss.org/
	GlassFish 				 	http://glassfish.java.net/
	Atmosphere 			 		https://github.com/Atmosphere/atmosphere
	Play Framework				http://www.playframework.com/
	Jetty					 	http://www.eclipse.org/jetty/
	jWebSocket				 	http://jwebsocket.org/
	Migratory data			 	http://migratorydata.com/
	Bristleback				 	http://bristleback.pl/

#### .NET

	Internet Information Services 8 http://www.iis.net/
	Fleck						https://github.com/statianzo/Fleck
	SuperWebSocket 				http://superwebsocket.codeplex.com/

#### PHP

	Php-websocket 				https://github.com/nicokaiser/phpwebsocket
	Rachet 						http://socketo.me/
	Hoar						https://github.com/hoaproject/Websocket

#### Python

	Tornado 					http://www.tornadoweb.org/en/stable/
	Pywebsocket					https://code.google.com/p/pywebsocket/
	Autobahn					http://autobahn.ws/
	txWS 						https://github.com/MostAwesomeDude/txWS
	WebSocket for Python 		https://github.com/Lawouach/WebSocketfor-Python 

#### Ruby

	EM-WebSocket				https://github.com/igrigorik/emwebsocket
	Socky server 				https://github.com/socky/socky-serverruby

#### JavaScript
这不是一个笑话，由于有Node.js你可以使用JavaScript创建web服务器。Node.js（http://nodejs.org）是一个事件驱动的框架，允许您构建实时Web应用程序，它也可以被Google的V8脚本引擎解释执行。虽然该框架不支持WebSockets开箱即用，但一些扩展还是相当不错的。

	Socket IO 					http://socket.io/
	WebSocket-Node 				https://github.com/Worlize/WebSocketNode
	Node WebSocket Server 		https://github.com/miksago/nodewebsocket-server 

Node.js的粉丝不断的增加，所以值得你去试一下。

#### 配置开发环境
创建服务器的环境取决于您打算使用的技术、框架和编程语言，这里有各种各样的集成开发环境(IDE)将使你的开发变得更加轻松！

下面是列出的是我们推荐使用的一些IDE，以及它们所支持的web编程语言：

<table>
<tr>
<th>IDE</th>
<th>操作系统</th>
<th>支持语言</th>
</tr>
<tr>
<td>Aptana</td>
<td>Windows,Mac,Linux</td>
<td>HTML5,JavaScript,C/C++,Java</td>
</tr>
<tr>
<td>NetBeans</td>
<td>Windows, Mac, Linux</td>
<td>HTML5，C/C++Java</td>
</tr>
<tr>
<td>Eclipse (有Web插件)</td>
<td>Windows, Mac, Linux</td>
<td>HTML5，JavaScript，C/C++，Java</td>
</tr>
<tr>
<td>Visual Studio</td>
<td>Windows</td>
<td>HTML5，JavaScript，.NET</td>
</tr>
<tr>
<td>WebMatrix</td>
<td>Windows</td>
<td>HTML5，JavaScript，PHP，.NET</td>
</tr>
<table>

通过这本书，我们决定使用C#.NET和Fleck，虽然这对你可能没什么区别，你随意挑选你喜欢的语言或者根据你现在项目的需求选择。

C#有以下的优势：

* 它使用.NET框架运行在Windows平台上并且在Mac和Linux使用Mono
* 它有一个活跃的开发者社区，你可以方便的找到支持的东西
* 方便学习
* 你可以在几分钟之内快速的配置一个WebSocket服务器

选中了Fleck库是有如下的原因：

*  它支持Windows和Unix操作系统
*  它的扩展库是非常方便使用和配置的
*  它非常方便维护以及有很好的文档

这是如何快速使用C#建立一个基于Fleck的WebSocket服务器

1. 下载 Visual Studio Express(它可以在[http://www.microsoft.com/visualstudio/eng/products/visualstudio-express-for-windows-desktop](http://www.microsoft.com/visualstudio/eng/products/visualstudio-express-for-windows-desktop)免费得到)
2. 下载Fleck([https://github.com/statianzo/Fleck]( (https://github.com/statianzo/Fleck)))
3. 启动Visual Studio并点击 File | New | Project
4. 在Visual C#下选择Windows
5. 选择Console Application(是的，一个基于控制台的服务器是最简单的方式来构建一个WebSocket服务器)
6. 给你的项目命名并点击OK
7. 在Solution Explorer选项卡上，右击References标签并选择Add new reference
8. 点击browse并找到Fleck.dll文件
9. 点击OK按钮然后你已经完成了！

#### 连接到Web服务器

